#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Wen-Ting'
SITENAME = 'My website using GitLab Pages'
SITEURL = 'https://Wen-Ting.gitlab.io/pelican'

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'Europe/Athens'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('countdown', 'https://tw.piliapp.com/timer/countdown/'),
         ('airitilibrary', 'http://www.airitilibrary.com/'),
         ('airitilibrary', 'http://www.airitilibrary.com/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('airitilibrary', 'http://www.airitilibrary.com/'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
