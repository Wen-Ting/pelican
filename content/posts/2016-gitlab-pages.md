Title: 法律與科技知識筆記區
Date: 2020-08-11
Category: lawtech
Tags: lawtech, blank
Slug: pelican-on-lawtech-pages
Author: Wen-Ting



### 科技能否取代律師？  
* Legal Technology Track: Data Analytics in Law: <https://www.youtube.com/watch?v=iNafjrqvnww> 
* 律師的諮詢怎麼收費？
	* It depends!?  
	* 一間龍蝦餐廳，價格若標示"It depends"， <font color="#660066">客人買單嗎</font><br />   
   	* 經驗<font color="#00dd00">vs</font> 資料科學
	
  
  
  


### Image

![Clio Cloud Conference.](https://gitlab.com/Wen-Ting/pelican/-/blob/984341e12e7909dc5443675feafbbb46d8a96321/content/sample1.png "sample1.")

.
.
.
.
.


<font color="#00dd00">文字顏色更改(應該要是綠色的)</font><br /> 
<font color="#660066">文字顏色更改(應該要是深紫色的)</font><br /> 
<font size="3">字體大小為3 (結果完全看不出變化)</font><br /> 
<table><tr><td bgcolor=#D1EEEE>背景改成淺藍色：#D1EEEE</td></tr></table>

|  表頭   | 表頭  |
|  ----  | ----  |
| 單元格  | 單元格 |
| 單元格  | 單元格 |
